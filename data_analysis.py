import pandas as pd
import glob
import seaborn as sns

path = r'data/import'
all_files = glob.glob(path + "/*.csv")

li = []

for filename in all_files:
    df = pd.read_csv(filename, sep = ";", header = None, skiprows = 9, decimal=',')
    li.append(df)

frame = pd.concat(li, axis=0, ignore_index=True)

frame = frame.loc[frame[0] == 'SP']
frame_numeric = pd.to_numeric(frame[5], errors='coerce')


sns.scatterplot(3, 5, data=frame)
