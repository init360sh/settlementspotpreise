import os
import csv
import openpyxl
import openpyxl.styles
import datetime
from collections import namedtuple
from objects import SettlementPrice


class FileWriter:
    options = {

    }

    def __init__(self, path=None, options=None):
        self.path = path
        if options is not None:
            self.options.update(options)

    def write_xls(self, data):
        print("WRITE", self.path)
        folder_path = os.path.split(self.path)[0]
        if not os.path.exists(folder_path):
            os.mkdir(folder_path)
        wb = openpyxl.Workbook()
        wb.remove(wb.active)
        for type in data:
            sh = wb.create_sheet(title=type)
            sh.column_dimensions['A'].width = 30
            sh.column_dimensions['B'].width = 30
            self.__write_header(sh)
            for row_idx in range(0, len(data[type])):
                # notice! rows starting at 1 with openpyxl
                sh.cell(row=(row_idx + 2), column=1).value = data[type][row_idx].trading_day
                sh.cell(row=(row_idx + 2), column=1).number_format = 'DD.MM.YYYY'
                sh.cell(row=(row_idx + 2), column=2).value = data[type][row_idx].delivery_day_start
                sh.cell(row=(row_idx + 2), column=2).number_format = 'DD.MM.YYYY'
                sh.cell(row=(row_idx + 2), column=3).value = data[type][row_idx].price
                sh.cell(row=(row_idx + 2), column=4).value = data[type][row_idx].unit
        wb.save(self.path)

    def __write_header(self, sheet):
        sheet.cell(row=1, column=1).value = "Trading Day"
        sheet.cell(row=1, column=2).value = "Delivery Day"
        sheet.cell(row=1, column=3).value = "Settlement Price"
        sheet.cell(row=1, column=4).value = "Unit"


class FileReader:
    valid_file_types = ["xls", "xlsx", "csv"]
    options = {
        "do_backup": False
    }

    def __init__(self, path=None, options=None, backup_path=None):
        self.path = path
        self.backup_path = backup_path
        if options is not None:
            self.options.update(options)

    def read_many(self):
        print("READ MANY", self.path)
        all_items = []
        for filename in os.listdir(self.path):
            if self.__is_valid_file_type(self.path + filename):
                items = self.read(self.path + filename)
                all_items = all_items + items
        return all_items

    def read(self, path):
        if path is None:
            path = self.path
        print("READ", path)
        file_extension = os.path.splitext(path.lower())[1]
        if file_extension == '.xls' or file_extension == '.xlsx':
            return self.read_xls(path)
        elif file_extension == '.csv':
            return self.read_csv(path)

    def read_xls(self, path, row_type='SettlementPrice'):
        # TODO: SettlementPrice als Typen mitgeben und daraus entsprechend die Items instanziieren.
        # --> Item = __import__(row_type) --> SettlementPrice
        sheets_with_items = {}
        wb = openpyxl.load_workbook(path)
        sheet_names = wb.sheetnames
        for sheet_name in sheet_names:
            sh = wb[sheet_name]
            items = []
            # TODO: Erste Zeile ignorieren
            for row in sh.iter_rows():
                trading_day = row[0].value
                delivery_day = row[1].value
                price = row[2].value
                unit = row[3].value
                items.append(
                    SettlementPrice(None, sheet_name, None, trading_day, delivery_day, delivery_day, price, unit))
            sheets_with_items[sheet_name] = items
        return sheets_with_items

    def __is_valid_file_type(self, path):
        for valid_type in self.valid_file_types:
            if path.find(valid_type) != -1:
                return True
        return False

    def read_csv(self, path, row_type='SettlementPrice'):
        items = []
        with open(path) as csv_file:
            # csv_header = filter(self.__is_sp_header_row, csv_file)
            # csv_header_items = self.__read_header_items(csv_header)
            # TODO: translate header_items for "row_type" from csv_header_items or use "row_type" fields
            # header_items = ["data_type", "market_area", "long_name", "trading_day", "delivery_day_start", "delivery_day_end", "price", "unit"]

            trading_day_row = filter(self.__is_st_row, csv_file)
            trading_day_data = csv.reader(trading_day_row, delimiter=';')
            trading_day_item = []
            for trading_iter in trading_day_data:
                trading_iter = filter(self.__is_no_empty_cell, trading_iter)
                trading_day_item = list(map(self.__format_simple_dates, trading_iter))
                break

            data = filter(self.__is_sp_row, csv_file)
            csv_items = csv.reader(data, delimiter=';')
            for row in csv_items:
                row = filter(self.__is_no_empty_cell, row)
                row = list(map(self.__format_dates, row))
                # item = namedtuple(row_type, header_items)(*row)
                try:
                    item = SettlementPrice(row[0], row[1], row[2], trading_day_item[1], row[3], row[4], row[5], row[6])
                    items.append(item)
                except IndexError as e:
                    print("Error with csv row. No valid items given. Details: ", e)
                    print("Will skip row ", row)
                    # continue -> probably not needed

        if self.options["do_backup"]:
            if not os.path.exists(self.backup_path):
                os.mkdir(self.backup_path)
            os.rename(path, (self.backup_path + os.path.split(path)[1]))

        return items

    def __is_sp_row(self, r):
        return r.startswith("SP")

    def __is_st_row(self, r):
        # in this row is trading_day defined
        return r.startswith("ST")

    def __is_sp_header_row(self, r):
        return r.startswith("# Data type(SP)")

    def __is_no_empty_cell(self, c):
        return c != ""

    def __format_dates(self, c):
        try:
            val = datetime.datetime.strptime(c, '%Y-%m-%dT%H:%M:%SZ')
            return val
        except ValueError as e:
            return c

    def __format_simple_dates(self, c):
        try:
            val = datetime.datetime.strptime(c, '%Y-%m-%d')
            return val
        except ValueError as e:
            return c

    def __read_header_items(self, header):
        csv_header_row = csv.DictReader(header, delimiter=';')
        return csv_header_row.fieldnames
