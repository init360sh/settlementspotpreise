# Settlement Spot Preise

*Challenge von Vinson Meier aus dem Bereich EG.*

## AUSGANGSLAGE

Bis Mitte Dezember 2019 waren die Spot-Preise für Erdgas täglich, inklusive aller historischen Werte für das Kalenderjahr in einem File als Download der EEX verfügbar. 

Mit dem geänderten Download (via SFTP) seit Mitte Dezember 2019 werden die Spot-Preise nur noch für den betreffenden Liefertag gemeldet (pro Tag 1 File). Die historischen Werte werden nicht mehr mitgeliefert.

Die Spotpreise wurden in 2 Files (ein separates File für die Preise PEG) eingetragen und als Archiv und Datenquelle genutzt. Die Verwendung der Preise (Ermittlung von Preissignalen, Abbildung Markt-Spreads) findet jeweils separat und nach Verwendungszweck statt.

### ZIELE
    
- Erstellen eines Preisfiles für die Settlement Spot-Preise im Marktraum NCG gemäss neuem Download der EEX. Das File umfasst jeweils alle Werte des laufenden Kalenderjahres und dient als Archiv und Datenquelle.

- Extrahieren der Settlement Spot-Preise Erdgas vom FTP Server der EEX. 

- Um den zeitlichen Aufwand in einem vernünftigen Rahmen zu halten, werden nur die Spotpreise für den Marktraum NCG verwendet. Das Vorgehen für die übrigen Markräume (Gaspool, TTF, PEG, PSV) ist identisch mit dem Vorgehen für die NCG.

- Eintrag der Spot-Preise in ein neues Preisfile `Gas_Spot_SettlementHistory_2020.xlsx` mit der Tabellenbezeichnung `NCG`.
   
- Notwendige Angaben:
    - Bezeichnung des MarktraumesHandelstag (Trading Day)
    - Liefertag (Delivery Day)
    - Settlement Price in EUR/MWh
    - Kann der Download ohne manuelles Anstossen passieren? Täglicher Download und Eintrag im Preisfile jeweils um 08:00 hrs o.ä.
    - Falls der Download manuell angestossen wird: Eine Option, den Zeitraum angeben zu können (von...bis) nach Wochenenden und längeren Abwesenheiten.
    - Besteht die Möglichkeit den Download und Eintrag jeweils rückwirkend für die letzten 3 oder 5 Tage vorzunehmen?

## RESULTAT

Eine Präsentation der aktuellen, fertig aufbereiteten Daten findet sich hier: [analytics.energie360.ch](https://analytics.energie360.ch/settlementspotpreise/){target="_blank"}

## VORGEHEN

- Einführung von pipenv: Für dieses Projekt werden wir nicht nur die Basis-Packages von Python bnötigen, sondern auch weitere Packages, welche sich mit dem Package-Manager pip installieren lassen. Da pip eine Installation immer global durchführt ist es bei Python üblich, mit sogenannten virtuellen Umgebungen (virtual enironments) zu arbeiten. Dies ermöglicht, dass man in jedem Projekt immer mit genau den Packages in genau der Version arbeitet, die man benötigt. So entstehen keine Konflikte zwischen mehreren lokalen Projekten. Mehr zu pipenv erfahrt ihr unter https://pipenv-fork.readthedocs.io/en/latest/ 

- Verwenden des Packages python-dotenv: Für die Ausführung des Programms werden wir die Zugangsdaten zum FTP Server benötigen. Solche Zugangsdaten auf einem zentralen Repository abzulegen ist unsicher, da bei einer unkontrollierten Weitergabe des Sourcecode auch die vetraulichen Zugangsdaten weitergereicht werden. Eventuell gibt es auch Parameter, welche sich zwischen Entwicklung und Produktion unterscheiden. Wir führen deshalb das Package python-dotenv ein: Im Repository liegt nur noch eine Beispiel-Konfigurationsdatei `.env.example`. 
    - Vor der Ausführung des Programmcodes muss man diese Datei in `.env` umbenennen und mit den korrekten Werten versehen. (Diese kann man von Vinson beziehen.)
    - In der `.gitignore` Datei wird sichergestellt, dass die `.env` Datei nicht versioniert und damit nicht committed und auf Bitbucket hochgeladen wird. 
    - Zukünftig könnten verschiedene .env Files verwendet werden beispielsweise um zwischen einem Test- und einem Production-Environment zu unterscheiden. 
    - Weitere Dokumentation liegt unter https://pypi.org/project/python-dotenv/

- Verwenden des Packages pysftp: Die Standard FTP Bilbiothek von Python kann nicht mit Secure FTP umgehen, wir verwenden die bekannteste externe Library. Hinweis: Aus dem internen Netz gibt es immer wieder Probleme mit der Gültigkeit von SSH Keys an externe Server. Aus diesem Grund erlauben wir vorerst alle Zertifikate, was ein potentielles Sicherheitsrisiko darstellen könnte. 

- Verwenden des Package openpyxl: Hiermit schreiben wir statt einfacher Textfiles direkt Excel-Files. 

- Erstellen des Codes für den automatisierten Download der Files.

- Erstellen des Codes für den automatisierten Merge der Files.

## Dokumentation

### Voraussetzungen

- Installiertes Python Version 3.8, ansonsten Download und Installieren von https://python.org (sofern nicht über lokales Package-System verfügbar). Das verwenden von älteren Versionen der Python 3 Linie könnte zu Fehlern führen. Bei Bedarf: Mit pyenv können mehrere Python Version lokal installiert und einfach gewechselt werden. Dies wird nur benötigt, falls die verwendete Python Version 3.8 nicht anderweitig auf dem System installiert werden kann. https://github.com/pyenv/pyenv
  
- Installiertes pipenv, ansonsten installieren mit `pip install pipenv`

### Installation Lokal

1. `git clone` ausführen, siehe Clone this repository.
2. `pipenv install` zum Installieren des Python Environments. 
3. Das Environment File anlegen mit `cp .env.example .env` und die fehlenden Variablen eintragen.

### Installation Produktion

Neuen User anlegen:

* `sudo adduser settlementspotpreise`
* `sudo su - settlementspotpreise`


SSH Key generieren.

* `ssh-keygen`

Diesen SSH Key ausgeben und auf Bitbucket hinterlegen. 

* `cat ~/.ssh/id_rsa.pub`

Programmcode von Bitbucket beziehen.

* `cd prod/`
* `git clone git@bitbucket.org:init360sh/settlementspotpreise.git`

Da nicht sicher eine Python 3.8 Umgebung vorhanden ist beziehen und aktivieren wir diese über pyenv. 
Hierfür müssen im Vorfeld noch ein paar Libraries für Ubuntu installiert werden. Diese Befehle beziehen sich auf ein frisch installiertes Ubuntu 18.10. 
Andere Distributionen oder Versionen können abweichende Pakete zur Kompilierung von Python benötigen. 

* `cd settlementspotpreise/`
* `sudo apt install libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev`
* `sudo apt install libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev`
* `curl https://pyenv.run | bash`
* `pyenv install 3.8.5`
* `pyenv shell 3.8.5`
* `/home/settlementspotpreise/.pyenv/versions/3.8.5/bin/python3.8 -m pip install --upgrade pip`

Nun installieren wir als erstes pipenv. 

* `pip install pipenv`

Und mit pipenv die Projekt-spezifischen Pakete.

* `pipenv install`

Nur noch das .env File anpassen und es kann losgehen.

* `vi .env` oder `emacs .env` oder ein Kommandozeilen Editor eurer Wahl.

Vor der Ausführung mit `pipenv run` nie vergessen, dass aktive Python mit `pyenv shell 3.8.5` zu setzen, da ansonsten ggf. eine nicht kompatible Python Version verwendet wird. 

### Ausführen

#### Files abholen

`pipenv run fetch` holt die Files gemäss den Settings in .env. Mit dem Kommandozeilen-Parameter `interactive` werden die Variablen abgefragt. 

`pipenv run concat` liest alle heruntergeladenen Files aus und schreibt ein Excel mit den Daten.

### Visualisierung in R und shiny

Für die Darstellung der aufbereiteten Daten wird die Skriptsprache R verwendet. Sie eignet sich besonders für Aufgaben der Statistik und Datenbearbeitung. Das Paket [shiny](https://shiny.rstudio.com/){target="_blank"} ermöglicht eine interaktive Darstellung, d.h. die Präsentation der Daten kann mit verschiedenen Inputs wie Auswahlfelder oder Schieberegler modifiziert werden.
Weil diese shiny-App auf einem eigenen Server läuft, muss zuerst das fertige Excel-File via ssh geholt werden. Hier ist wie schon zuvor wichtig, dass Passwörter als lokale Variable hinterlegt werden, und nicht direkt im Code auftauchen.

Die eingelesenen Daten werden grafisch dargestellt, das geht sehr gut mit  [ggplot2](https://ggplot2.tidyverse.org/){target="_blank"}. Der Plot wird als shiny-Output definiert, während verschiedene shiny-Inputs die Interaktivität ermöglichen.
 
