from filehandler_classic import FileReader
from filehandler_classic import FileWriter
import os.path
import datetime
from objects import SettlementPrice


def post_process_items(items):
    categorized_items = {}
    for item in items:
        if item.market_area not in categorized_items:
            categorized_items[item.market_area] = []
        single_entries = resolve_dates_to_single_entries(item)
        for single_entry in single_entries:
            categorized_items[item.market_area].append(single_entry)
        categorized_items[item.market_area] = sorted(categorized_items[item.market_area],
                                                     key=lambda entry: entry.delivery_day_start)
    return categorized_items


def resolve_dates_to_single_entries(item):
    if item.long_name == "WITHIN-DAY":
        return [item]
    elif item.long_name == "DAY 1 MW":
        items = []
        delta = item.delivery_day_end - item.delivery_day_start  # as timedelta
        for i in range(delta.days + 1):
            day = item.delivery_day_start + datetime.timedelta(days=i)
            new_item = SettlementPrice(item.data_type, item.market_area, item.long_name, item.trading_day, day, day,
                                       item.price,
                                       item.unit)
            items.append(new_item)
        return items
    elif item.long_name == "WEEKEND 1 MW":
        items = []
        delta = item.delivery_day_end - item.delivery_day_start  # as timedelta
        for i in range(delta.days + 1):
            day = item.delivery_day_start + datetime.timedelta(days=i)
            new_item = SettlementPrice(item.data_type, item.market_area, item.long_name, item.trading_day,
                                       day, day,
                                       item.price, item.unit)
            items.append(new_item)
        return items
    return []


def main():
    import_path = "./data/import/"
    export_path = "./data/export/"
    new_file_name = "complete-list.xlsx"
    backup_path = "./data/import/_bak/"

    # read new ftp files
    ftp_file_reader = FileReader(import_path, None, backup_path)
    ftp_items = ftp_file_reader.read_many()
    ftp_items = post_process_items(ftp_items)

    writer = FileWriter(export_path + new_file_name)
    if not os.path.isfile((export_path + new_file_name)):
        # write base file
        writer.write_xls(ftp_items)
    else:
        # read base file and concat ftp file items
        base_reader = FileReader(export_path + new_file_name)
        base_items = base_reader.read(None)
        # all_items = base_items.update(ftp_items)
        all_items = {**base_items, **ftp_items}
        writer.write_xls(all_items)


if __name__ == "__main__":
    main()
