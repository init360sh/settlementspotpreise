# settings.py
import os
from pathlib import Path
from dotenv import load_dotenv
load_dotenv()

SERVER = os.getenv("SERVER")
FILEPATH = os.getenv("FILEPATH")
DOWNLOADDIR = os.getenv("DOWNLOADDIR")
USERNAME = os.getenv("USERNAME")
PASSWORD = os.getenv("PASSWORD")
SCOPE = os.getenv("SCOPE")
FILETYPES = os.getenv("FILETYPES")
REFETCH= os.getenv("REFETCH")

if __name__ == "__main__":
    print("SERVER      %s" % SERVER)
    print("FILEPATH    %s" % FILEPATH)
    print("DOWNLOADDIR %s" % DOWNLOADDIR)
    print("USERNAME    %s" % USERNAME)
    print("PASSWORD    %s" % PASSWORD)
    print("SCOPE       %s" % SCOPE)
    print("FILETYPES   %s" % FILETYPES)
    print("REFETCH     %s" % REFETCH)
