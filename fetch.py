import pysftp  # https://pysftp.readthedocs.io/en/release_0.2.9/
import settings
from datetime import date, datetime, timedelta
import logging
import os
import sys

# General logging, everything unspecified will go to debug.log (will be overwritten with every run!)
logging.basicConfig(filename='logs/debug.log', filemode='w', level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%d-%m-%Y %H:%M:%S')

logger = logging.getLogger(__name__)

# Print everything onto the console
log_console = logging.StreamHandler()
log_console.setLevel(logging.DEBUG)
log_console.setFormatter(logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                           datefmt='%d-%m-%Y %H:%M:%S'))
logger.addHandler(log_console)

# Write the important stuff into a file, one file a day
#log_file = logging.FileHandler('/logs/%s.log' % date.today().day, mode='w')
# log_file.setLevel(logging.INFO)
# logger.addHandler(log_file)

class FileLister(object):
    """
    Contains functions to create lists of validated, existing files. 
        :param object: 
    """

    def __init__(self, sftp: pysftp.Connection, dirtype: str, filetype: str, refetch: bool):
        """
            :param self: 
            :param sftp:pysftp.Connection: 
        """
        self.dirtype = dirtype 
        self.filetype = filetype
        self.sftp = sftp
        self.refetch = refetch

    def _file_exists(self, filename: str) -> bool:
        file_exists= os.path.isfile(os.path.join(os.path.dirname(os.path.abspath(__file__)), 
                                    settings.DOWNLOADDIR, 
                                    os.path.basename(filename)))

        return file_exists

    def _filename_by_days_passed(self, days_passed: int) -> str:
        """
        All files are available based on a fixed path and filename pattern based on the date.
        This function returns the full path to a file based on the days passed.  
            :param self: 
            :param days_passed:int: Days back in time from today. 1 would be yesterday.
        """
        day = date.today() - timedelta(days=days_passed)
        dates = (
            day.strftime('%Y'),
            day.strftime('%Y%m%d'),
            day.strftime('%Y%m%d'),
        )
        filename = f'/market_data/natgas/{self.dirtype}/spot/csv/{dates[0]}/{dates[1]}/GasSpotMarketResults_{self.filetype.upper()}_{dates[2]}.csv'
        return filename

    def get_filelist_crawler(self) -> list:
        """
        Crawles all subdirectories and looks for files based on a specific filename pattern.
            :param self: 
        """
        # Prüft rekursiv alle Verzeichnisse und Dateien und fügt die passenden der Liste hinzu.
        # Diese Operation ist langsam (> 3 Minuten)

        filelist = []

        def _file(url):
            if f'GasSpotMarketResults_{self.filetype.upper()}_' in url:
                if not self.refetch and self._file_exists(url):
                    logger.debug(f'[-] File exists locally: {url}')
                else:
                    logger.debug(f'[X] File found on server: {url}')
                    filelist.append(url)

        def _dir(url):
            pass

        def _other(url):
            pass

        self.sftp.walktree(settings.FILEPATH,
                           _file, _dir, _other, recurse=True)

        return filelist

    def get_filelist_by_days(self, days_back: int, start: int = 0) -> list:
        """
        Returns all files based on a specific filename pattern max days from today. 
            :param self: 
            :param days_back:int: Days back from today (start=0)
            :param start:int=0: Days back from this day.
        """
        # Alle Files basierend auf einem fixen Verzeichnis und Dateinamem-Muster.
        # Nur Dateien die auch existieren werden zurückgemeldet.
        filelist = []
        for i in range(start, days_back):
            filename = self._filename_by_days_passed(i)
            if not self.refetch and self._file_exists(filename):
                logger.debug(f'[-] File exists locally: {filename}')
                continue
            if self.sftp.exists(filename):
                filelist.append(filename)
                logger.debug(f'[X] File found on server: {filename}')
            else:
                logger.warning(f'[!] File not found on server: {filename}')

        return filelist

    def get_filelist_yesterday(self) -> list:
        """
        Returns the file list with only the file from yesterday.
            :param self: 
        """
        return self.get_filelist_by_days(1)

    def get_filelist_lastweek(self) -> list:
        """
        Returns the file list with only the file from last week.
            :param self: 
        """
        return self.get_filelist_by_days(7)

    def get_filelist_current_year(self) -> list:
        """
        Returns all the files since the start of the current year.
            :param self: 
        """
        day_of_the_year = datetime.today().timetuple().tm_yday
        return self.get_filelist_by_days(day_of_the_year)


class FileFetcher(object):

    def __init__(self, sftp):
        # sftp: Die Klasse erwartet eine offene Verbindung zu einem SFTP Server
        self.sftp = sftp

    def fetch(self, filelist):
        os.chdir(os.path.join(os.path.dirname(os.path.abspath(__file__)), settings.DOWNLOADDIR))
        for filename in filelist:
            logger.debug(f'[o] Fetching: {filename}')
            self.sftp.get(filename) 
        os.chdir(os.path.join(os.path.dirname(os.path.abspath(__file__))))


def fetch(interactive=False):
    # Scope has to be (a)ll, this (y)ear, yester(d)ay
    # Functions falls back to direct (console) input if no sufficient option was set.
    if interactive:
        filetypes=''
        while not filetypes:
            filetypes = input('Please tell me which filetypes to fetch (gpl:gaspool,ncg:ncg,peg:peg,ttf:ttf): ')

        scope=''
        while not scope in ('a', 'y', 'd', 'w'):
            scope = input('Please tell me which files to fetch (a)ll, this (y)ear, yester(d)ay, last (w)eek: ')
        refetch=''
        while not refetch in ('y', 'n'):
            refetch = input('Refetch files that are already available local (y)es, (n)o: ')
    else:
        filetypes = settings.FILETYPES
        scope = settings.SCOPE
        refetch = settings.REFETCH

    refetch = refetch == 'y'

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    logger.debug(f'Open connection to {settings.SERVER}.')

    with pysftp.Connection(host=settings.SERVER,
                           username=settings.USERNAME,
                           password=settings.PASSWORD,
                           cnopts=cnopts) as sftp:
       
        filelist = []

        for filetype in filetypes.split(','):

            fl = FileLister(sftp, filetype.split(':')[0], filetype.split(':')[1], refetch)
            if scope == 'a':
                filelist += fl.get_filelist_crawler()
            elif scope == 'y':
                filelist += fl.get_filelist_current_year()
            elif scope == 'd':
                filelist += fl.get_filelist_yesterday()
            elif scope == 'w':
                filelist += fl.get_filelist_lastweek()
            else:
                logger.warning(f'Could not identify scope {scope}. Abort.')
                filelist = []

        ff = FileFetcher(sftp)
        ff.fetch(filelist)


if __name__ == "__main__":
    fetch('interactive' in sys.argv)
