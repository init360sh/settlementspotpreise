import datetime


# Data type(SP)	    Market Area	    Long Name	Delivery Start	Delivery End	Settlement Price	Unit of Prices
class SettlementPrice:
    def __init__(self, data_type='SP', market_area=None, long_name=None, trading_day=None, delivery_day_start=None,
                 delivery_day_end=None, price=0, unit='EUR/MWh'):
        # TODO: Als Typ ("Class.forName()") in FileReader fuer xls mitgeben.
        self.data_type = data_type
        self.market_area = market_area
        self.long_name = long_name
        # self.delivery_day_start = datetime.datetime.strptime(delivery_day_start, '%Y-%m-%dT%H:%M:%S:%fZ').date()
        # self.delivery_day_end = datetime.datetime.strptime(delivery_day_end, '%Y-%m-%dT%H:%M:%S:%fZ').date()
        self.trading_day = trading_day
        self.delivery_day_start = delivery_day_start
        self.delivery_day_end = delivery_day_end
        self.price = price
        self.unit = unit
